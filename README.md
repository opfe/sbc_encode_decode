# sbc_encode_decode

#### 介绍
蓝牙a2dp协议中的sbc编码 解码器，在windows下运行的工具软件，含编解码资源。

#### 使用说明

1.  cmd下，.\sbc_decoder.exe -h
2.  .\sbc_decoder.exe -ottt.wav ttt.sbc
3.  xxx


#### bludroid的sbc编解码器

源码很简单，简单说下，没时间专门做例子了。

https://gitee.com/EspressifSystems/esp-idf/tree/release/v5.0/components/bt/host/bluedroid/external/sbc

这个文件夹里的代码足够，顶多有几个宏需要预定义一下。


OI_CODEC_SBC_DecoderReset() 和 OI_CODEC_SBC_DecodeFrame()完成解码，把sbc文件切碎喂进去就行，就输出pcm数据


extern void SBC_Encoder(SBC_ENC_PARAMS *strEncParams);
extern void SBC_Encoder_Init(SBC_ENC_PARAMS *strEncParams);
完成编码，结构体里要填好内容


对比https://gitee.com/EspressifSystems/esp-idf/tree/release/v5.0/components/bt/host/bluedroid里面对这两个api的使用就能看懂用法